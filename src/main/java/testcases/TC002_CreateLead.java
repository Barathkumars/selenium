package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "CreateLead";
		authors = "Barath";
		category = "smoke";
		dataSheetName = "TC002";
		testNodes = "Leads";
			
	}
	
	@Test(dataProvider = "fetchData")
	public void CreateLead(String userName,String password,String CName, String FName,String LName) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickcreateLead()
		.enterCName(CName)
		.enterFirstName(FName)
		.enterLastName(LName);
		
		
	}
}