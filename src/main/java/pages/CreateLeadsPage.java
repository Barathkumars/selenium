package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadsPage extends ProjectMethods{

	public CreateLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "createLeadForm_companyName") WebElement CName;
	@FindBy(id = "createLeadForm_firstName") WebElement FName;
	@FindBy(id = "createLeadForm_lastName") WebElement LName;
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit") WebElement ClickCreateLead;
	
	public CreateLeadsPage enterCName(String data) {
		type(CName, data);
		return this;
	}
	public CreateLeadsPage enterFirstName(String data) {
		type(FName, data);
		return this;
	}
	public CreateLeadsPage enterLastName(String data) {
		type(LName, data);
		return this;
	}

	public ViewLeadPage clickCreateLead() {		
		click(ClickCreateLead);
		return new ViewLeadPage();
	}




}
