package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{
	@FindBy(how = How.CLASS_NAME, using = "linktext") WebElement ClickLeadId;
	
	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	public MergeLeadsPage ClickLeadId() {		
		click(ClickLeadId);
		return new MergeLeadsPage();
	}




}
