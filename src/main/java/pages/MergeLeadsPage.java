package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods{

	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "(//img[@alt='Lookup'])[1]") WebElement eleClickLookup;
	
	@FindBy(how = How.XPATH, using = "(//img[@alt='Lookup'])[2]") WebElement eleClickLookup1;
	
/*	public MergeLeadsPage enterCName(String data) {
		type(CName, data);
		return this;
	}
	public MergeLeadsPage enterFirstName(String data) {
		type(FName, data);
		return this;
	}
	public MergeLeadsPage enterLastName(String data) {
		type(LName, data);
		return this;
	}*/

	public FindLeadsPage ClickLookup() {		
		click(eleClickLookup);
		return new FindLeadsPage();
	}

	public FindLeadsPage ClickLookup1() {		
		click(eleClickLookup1);
		return new FindLeadsPage();
	}


}
