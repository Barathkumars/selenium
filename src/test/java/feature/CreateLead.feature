Feature: To Create Lead

Background:
Given Open the Browser
And Max theBrowser
And Set the Timeout
And Launch the URL

Scenario Outline: Can Create a Lead
And Enter the User name as <userName>
And Enter the Password as <password>
And Click on the Login Button
And Click the CRMSFA
And Click on Leads Button
And Click on Create Lead Button
And Enter the Company Name as <CName>
And Enter the First Name as <FName>
And Enter the Last Name as <LName>
And Enter the First Name as <FName>
When Click the CreateLead Button   
Then Create Lead Successfully

Examples:
|userName|password|CName|FName|LName|
|DemoSalesManager|crmsfa|Nltd|Barath|S|






