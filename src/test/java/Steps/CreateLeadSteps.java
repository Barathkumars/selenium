package Steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;

public class CreateLeadSteps {
	ChromeDriver driver;
	@Given("Open the Browser")
	public void open_the_Browser() {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();

	}

	@Given("Max theBrowser")
	public void max_theBrowser() {
		driver.manage().window().maximize();
	}

	@Given("Set the Timeout")
	public void setTimeOut()
	{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}


	@Given("Launch the URL")
	public void launch_the_URL() {
		// Write code here that turns the phrase above into concrete actions
		driver.get("http://leaftaps.com/opentaps/control/main");
	}

	@Given("Enter the User name as (.*)")
	public void enter_the_User_name(String username) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("username").sendKeys(username);
	}

	@Given("Enter the Password as (.*)")
	public void enter_the_Password(String password) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("password").sendKeys(password);
	}

	@Given("Click on the Login Button")
	public void click_on_the_Login_Button() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Given("Click the CRMSFA")
	public void click_the_CRM_SFA() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("Click on Leads Button")
	public void click_on_Leads_Button() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Leads").click();
	}

	@Given("Click on Create Lead Button")
	public void click_on_Create_Lead_Button() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Create Lead").click();
	}

	@Given("Enter the Company Name as (.*)")
	public void enter_the_Company_Name_as_NLTD(String CName) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_companyName").sendKeys(CName);
	}

	@Given("Enter the First Name as (.*)")
	public void enter_the_First_Name(String FName) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_firstName").sendKeys(FName);
	}

	@Given("Enter the Last Name as (.*)")
	public void enter_the_Last_Name(String LName) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_lastName").sendKeys(LName);
	}
	@Given("Click the CreateLead Button")
	public void click_the_CreateLead_Button() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByClassName("smallSubmit").click();
	}

	@Given("Create Lead Successfully")
	public void create_Lead_Successfully() {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("Created Sucessfully");
	}



}
